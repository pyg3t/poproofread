"""File of tasks to be invoked with invoke"""

from invoke import task


@task
def clean(c, docs=False, bytecode=False, extra=""):
    """Clean the project"""
    patterns = ["dist"]
    if docs:
        patterns.append("docs/_build")
    if bytecode:
        patterns.append("**/*.pyc")
    if extra:
        patterns.append(extra)
    for pattern in patterns:
        c.run("rm -rf {}".format(pattern))
    print("Cleaned")


@task
def build(c):
    """Build the project packages"""
    c.run("python setup.py sdist bdist_wheel")
    print("Built")


@task
def upload_packages(c):
    """Upload packages to PyPI"""
    c.run("twine upload dist/*")
    print("Uploaded")
